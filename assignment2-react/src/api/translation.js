import { createHeaders } from ".";

const url = process.env.REACT_APP_API_URL;

//adding new translation
export const addTranslation = async (user, translation) => {

    //using js shift-method to remove the first element
    if (user.translations.length > 9) {
		user.translations.shift();
	}

    try{
        const response = await fetch(`${url}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                username: user.username,
                translations: [...user.translations, translation]
            })
        })

        if(!response.ok){
            throw new Error("Could not update the translation");
        }
        const result = await response.json();
        return [null, result];
    }
    catch(error){
        return [error.message, null];
    }
}

//Clearing list of translations
export const translationCleanList = async (userId) => {
    try{
        const response = await fetch(`${url}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if(!response.ok){
            throw new Error("Could not update the translations");
        }
        const result = await response.json();
        return [null, result];
    }
    catch(error){
        return [error.message, null];
    }

}