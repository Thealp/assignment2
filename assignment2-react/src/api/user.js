import {createHeaders} from './index';

const url = process.env.REACT_APP_API_URL;

export const checkForUser = async (username) => {
    try{
        
        const resp = await fetch(`${url}?username=${username}`);
        if(!resp.ok){
            throw new Error('Could not complete request');
        }
        const data = await resp.json()
        return [null, data];
    }
    catch (error){
        return [error.message, []];
    }
}

export const createUser = async (username) => {
    try{
        
        const resp = await fetch(url, {
            method: 'POST', // create a resource
            headers: createHeaders(),
            body:JSON.stringify({
                username, 
                translations:[]
            })
        });

        if(!resp.ok){
            throw new Error('Could not create user with username ' + username);
        }
        const data = await resp.json();
        return [null, data];
    }
    catch (error){
        return [error.message, []];
    }
}

export const loginUser = async (username) =>{
    const [checkError, user] = await checkForUser(username);

    if(checkError !== null){
        return [checkError, null];
    }

    //if there is an user there
    if (user.length > 0){
        return [null, user.pop()];
    }
    //if user does not exists
    return await createUser(username);

}

export const findUserById = async(userId) => {
    try{
        const response = await fetch(`${url}/${userId}`)
        if(!response.ok){
            throw new Error("Could not fetch user");
        }
        const user = await response.json();
        return [null, user];
    }
    catch(error){
        return [error.message, null];

    }
}
