import { useEffect } from "react";
import { findUserById } from "../api/user";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileTranslationList from "../components/Profile/ProfileTranslationList";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";
import { Link} from "react-router-dom";

const Profile = () => {

    const {user, setUser} = useUser();

    useEffect(()=>{
        const findUser = async () => {
            const [error, latestUser] = await findUserById(user.id);
            if(error===null){
                storageSave(STORAGE_KEY_USER, latestUser);
                setUser(latestUser);
            }
        }
        findUser();
    }, [setUser, user.id])
    

    return(
        <>
        <div className="backgroundProfile">
            <Link to="/translation" className="backToTranslate">Back to Translation-page</Link>
            <h1 className="headerProfile">Profile</h1>
            <div className = "profileEl">
                <ProfileTranslationList translations={user.translations}/>
                <ProfileActions/>
                
            </div>
           
        </div>  
        </>
        
    );
}

//Protecting the route
export default withAuth(Profile);