import { useState } from "react";
import { addTranslation } from "../api/translation";
import TranslationForm from "../components/Translation/TranslationForm";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage";


const Translation = () => {

    const [signLanguage, setSignLanguage] = useState();
    const {user, setUser} = useUser();

    const handleBtnClicked = async (input) =>{

        const [error, result] = await addTranslation(user, input);
        if(error!==null){
            return;
        }
        //Keep UI state and Server state in sync
        storageSave(STORAGE_KEY_USER, result);
        //Update context state
        setUser(result);

        console.log("error: ", error);
        console.log("translation:", result);

        //translating to signlanguage
        const translation = input.split("").map((letter) =>{
            const lowcaseLetter = letter.toLowerCase();
            const img = "/img/"+ lowcaseLetter + ".png";
            console.log(img);
            return <img src={img} height="50" width="50"/>;
        });
        setSignLanguage(translation);
        console.log(signLanguage);

    };
    
    return(
        <>
            <section id="textInput">
                <TranslationForm onTranslation={handleBtnClicked}/>
            </section>
            <section className="signOutput" id="signOutput">
                {signLanguage}
            </section>
        </>
       
    );
}

//protecting the route. Not able to see if not logged in
export default withAuth(Translation);