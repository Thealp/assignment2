import LoginForm from "../components/Login/LoginForm";

const Login = () => {
    return(
        <>
            <div className="background">
                <img src="/img/Logo.png" alt="Logo" width="50" height="50"/>
                <div className="header">
                    <h1>Lost in Translation</h1>
                    <h2>Get started</h2>
                </div>
            </div>
            <LoginForm></LoginForm>
        </>
        
    )
}

export default Login;