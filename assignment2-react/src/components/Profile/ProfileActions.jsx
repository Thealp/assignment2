import { translationCleanList } from "../../api/translation";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageDelete, storageSave } from "../../utils/storage";
import './Profile.css'


const ProfileActions = () =>{

    const {user, setUser} = useUser();

    const handleLogoutClick = () => {
        if(window.confirm("Are you sure?")){
            //Send an event to the parent
            storageDelete(STORAGE_KEY_USER);
            setUser(null);
        }
    }

    const handleClearListClick = async () =>{

        if(window.confirm("Are you sure?\nThis can not be undone")){
            const [clearError] = await translationCleanList(user.id);

            if(clearError!==null){
                return
            }
            const updatedUser ={
                ...user,
                translations: []
            }
    
            storageSave(STORAGE_KEY_USER, updatedUser);
            setUser(updatedUser);
        }
    }

    //add redirect back
    return (
        <>
            <ul className="profileBtns">
            
                <li><button className="clearList" onClick={handleClearListClick}>Clear list</button></li>
                <li><button className="logout" onClick={handleLogoutClick}>Logout</button></li>
            </ul>
        </>
        
    );
}
export default ProfileActions;