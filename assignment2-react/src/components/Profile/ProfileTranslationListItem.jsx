const ProfileTranslationListItem = ({translation}) => {
    return <li>{translation}</li>
}

export default ProfileTranslationListItem;