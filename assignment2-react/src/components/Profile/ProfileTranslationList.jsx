import ProfileTranslationListItem from "./ProfileTranslationListItem";
import './Profile.css'

const ProfileTranslationList = ({translations}) =>{

    //index makes it possible to have duplicated translations
    const translationList = translations.map(
        (translation, index) => <ProfileTranslationListItem key = {index +"-"+ translation} translation={"- "+translation}/>)

   
    return (
        <section className="translationsBox">
            <h3> Your last translations:</h3>
            {translationList.length === 0 && <p>You have no translations yet</p>}
            <ul>
                {translationList}
            </ul>
        </section>
    );
}

export default ProfileTranslationList;