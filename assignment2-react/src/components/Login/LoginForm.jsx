import {useForm} from 'react-hook-form';
import { useState, useEffect } from "react";
import {loginUser} from '../../api/user';
import { storageSave } from '../../utils/storage';
import { useUser } from '../../context/UserContext';
import {useNavigate} from 'react-router-dom';
import { STORAGE_KEY_USER } from '../../const/storageKeys';
import './LoginForm.css'

const usernameConfig = {
    required: true,
    minLength: 3,
}

const LoginForm = () => {
    //Hooks
    const {register, handleSubmit, formState: {errors}} = useForm();
    const {user, setUser} = useUser();

    const navigate = useNavigate();

    //Local state
    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null); //used if something went wrong while trying to log in

    //Side Effects
    useEffect(() =>{
        if(user!==null){
            navigate('/translation');
        }
    }, [user, navigate])

    //Event handlers
    const onSubmit = async ({username}) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username);
        if(error !== null){
            setApiError(error);
        }
        if(userResponse !== null){
            storageSave(STORAGE_KEY_USER, userResponse);
            setUser(userResponse)
        }
        setLoading(false);
        
    }


    const errorMessage = (() => {
        if(!errors.username){
            return null;
        }
        if(errors.username.type === 'required'){
            return <span>Username is required</span>;
        }
        if(errors.username.type === 'minLength'){
            return <span>Username is too short (min 3)</span>
        }
    })()

    return(
        <div className='componentBody'>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset className="inputBox">
                    <input className="loginInput" type="text" placeholder='What`s your name?' id="username" {...register("username", usernameConfig)}/>
                    <button className="clickLogin" type="submit" disabled={loading}>&#8250;</button>
                    
                    
                </fieldset>
                {errorMessage}
            
                {loading && <p>Logging in...</p>}
                {apiError && <p>{apiError}</p>}
            </form>
        </div>
        
    );
}

export default LoginForm;