import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";

const Navbar = () => {

    const {user} = useUser();

    return(
        <nav className="navBar">
            <ul className="navHeader">
                <li>
                    <h2>Lost in translation</h2>
                </li>
            </ul>

            {user!==null &&
            <ul className="links">
                <li>
                    <NavLink to="/profile"><img src="img/icon-user.png" width="30px" height="30px"/></NavLink>
                </li>
            </ul>
            }
        </nav>
    );
}

export default Navbar;