import { useForm } from "react-hook-form";
import './TranslationForm.css';


//component for text-input
const TranslationForm = ({onTranslation}) =>{

    const {register, handleSubmit} = useForm();

    const onSubmit = ({data}) => {
        onTranslation(data);
       
    };

    return(
        <>
            <fieldset className="inputTranslation">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <input className="inputTranslate" placeholder="Write something" type="text" {...register('data')}/>
                    <button className="clickTranslate" type="submit">&#8250;</button>
                </form>
            </fieldset>
        </>
        
    );
}
export default TranslationForm;