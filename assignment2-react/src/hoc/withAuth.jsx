import { Navigate } from "react-router-dom";
import { useUser } from "../context/UserContext"

const withAuth = Component => props => {
    const {user} = useUser();

    //if the user is logged in
    if(user !== null){
        //forward the props
        return <Component  {...props}/>
    }
    //if the user is not logged in navigate to login-page
    else{
        return <Navigate to="/"/>
    }
}

export default withAuth;