# Assignment2: React


# Table of contents
- Background
- Usage
- Author
- License

# Background
Lost in Translation is a Single Page Application using React framework. It's an online sign language translator that takes a string of regular text as input and translates it to sign language. 
The application consists of a login-page, where user can write any name, that will be stored in an API, and then the translation-page will be displayed. To manage sessions, the browser's session storage is used.
On the translation-page, the user can write text in the input-field and get it translated to sign language. The translation-text will be stored in an API. The user can also redirect to the profile-page from the translation-page. 
When on the profile-page, the user can see their last 10 translations. These are fetched from the API. This page also gives the opportunity to clear all stored translations by pressing the "clear list"-button. From here the user can also log out, by pressing the "logout"-button, and will be redirected to the login-page. 

# Usage
To open the application click this link:
[SignLanguage-Application](https://tlp-assignment2-react.herokuapp.com/). It will now open in your browser.

To run this program by downloading the code you will need to install node. Run 'npm start' in terminal and the project should run on localhost. Make sure to be located in the right folder. 

# Author
Gitlab username: Thealp

Link: https://gitlab.com/Thealp

# License
MIT © Thea Lunder Pettersen



